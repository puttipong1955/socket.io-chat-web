const socket = io('http://localhost:3000');
const messageForm = document.getElementById('send-container')
const messageInput = document.getElementById('message-input')
const messageContainer = document.getElementById('message-container')

const name = prompt('What is your name?')
appandMessage('You Injoined')
socket.emit('new-user', name)

socket.on('user-connected', name => {
    appandMessage(`${name} connected`)
})

socket.on('user-disconnected', name => {
    appandMessage(`${name} disconnected`)
})

socket.on('chat-message', data => {
    appandMessage(`${data.name}: ${data.message}`)
})

messageForm.addEventListener('submit', e => {
    e.preventDefault()
    const message = messageInput.value
    appandMessage(`You: ${message}`)
    socket.emit('send-chat-message', message)
    messageInput.value = ''
})

function appandMessage(message) {
    const messageElement = document.createElement('div')
    messageElement.innerText = message
    messageContainer.append(messageElement)
}
